---
author: Mireille COILHAC
title: Mettre au même dénominateur
---


## I. Avec des nombres

!!! example "Observer et comprendre cet exemple"


    $\frac{2}{5} - \frac{3}{7} = \frac{2 \times 7}{5 \times 7} - \frac{3 \times 5}{7 \times 5} = \frac{2 \times 7 - 3 \times 5}{5 \times 7}$


[Additions et soustractions de fractions](https://coopmaths.fr/alea/?uuid=b51ec&id=2N30-2&v=eleve&es=011100&title=Additions+et+soustractions+de+fractions){ .md-button target="_blank" rel="noopener" }

!!! example "Observer et comprendre ces exemples"

    ![multiplier](multiplication_fractions.png){ width=30% }

    ![diviser](division_fractions.png){ width=30% }

[Multiplications et divisions de fractions](https://coopmaths.fr/alea/?uuid=29919&id=2N30-3&n=6&d=10&s=3&s2=false&s3=true&s4=3&cd=1&v=eleve&es=011100&title=Multiplications+et+divisions+de+fractionsions+de+fractions){ .md-button target="_blank" rel="noopener" }



## II. Avec des expressions littérales

On procède de la même façon qu'avec de simples nombres.

!!! example "Attention au signe **-** devant une fraction"

    ![soustraire](exemple.png){ width=90% }


    ![multiplier](exo_resolu.png){ width=100% }
    

[Mettre au même dénominateurs des expressions littérales](https://coopmaths.fr/alea/?uuid=641bc&id=2N41-8&n=10&d=10&s=2&cd=1&v=eleve&es=011100&title=Mettre+au+m%C3%AAme+d%C3%A9nominateur+des+expressions+litt%C3%A9rales){ .md-button target="_blank" rel="noopener" }




