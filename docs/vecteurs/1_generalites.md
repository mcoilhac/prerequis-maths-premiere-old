---
author: Mireille COILHAC
title: Vecteurs - introduction
---

## I. Visualiser

[Représenter un vecteur](https://www.geogebra.org/m/kn5fpvqp){ .md-button target="_blank" rel="noopener" }

[Coordonnées d'un vecteur - 1](https://www.geogebra.org/m/WsyahSJr#material/mJ2vR9hW){ .md-button target="_blank" rel="noopener" }

[Coordonnées d'un vecteur - 2](https://www.geogebra.org/m/WsyahSJr#material/S3ctxqWh){ .md-button target="_blank" rel="noopener" }

[Coordonnées d'un vecteur - 3](https://www.geogebra.org/m/d8dzpvpb){ .md-button target="_blank" rel="noopener" }

[Coordonnées d'un vecteur - 4](https://www.geogebra.org/m/WsyahSJr#material/DJafsaup){ .md-button target="_blank" rel="noopener" }

[Coordonnées d'un vecteur - 5](https://www.geogebra.org/m/bcqsu6rp){ .md-button target="_blank" rel="noopener" }

[Somme de deux vecteurs](https://www.geogebra.org/m/UvCVC8xf){ .md-button target="_blank" rel="noopener" }


## II. Exercices

[Premiers exercices sur les vecteurs](https://coopmaths.fr/alea/?uuid=3a3ec&id=2G22-1&uuid=f71c1&id=2G24-1&n=2&d=10&s=1&cd=1&uuid=49570&id=2G24-2&n=2&d=10&s=1&cd=1&uuid=14a2c&id=2G24-3&n=2&d=10&s=1&cd=1&uuid=68693&id=2G24-4&n=2&d=10&s=1&cd=1&uuid=222f6&id=2G24-5&n=2&d=10&s=1&cd=1&uuid=6b705&id=2G24-6&v=eleve&es=011100&title=Les+vecteurs+-+1){ .md-button target="_blank" rel="noopener" }

