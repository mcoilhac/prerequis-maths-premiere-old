---
author: Mireille COILHAC
title: Vecteurs - Propriétés
---

## I. La relation de Chasles

<iframe width="560" height="315" src="https://www.youtube.com/embed/PK1eRGHS7WI?si=0Iyfyuk9aXBRrlX2" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

[La reelation de Chasles](https://coopmaths.fr/alea/?uuid=26f3b&id=can2G10&n=5&d=10&cd=1&uuid=7bc4a&id=can2G11&n=5&d=10&cd=1&v=eleve&es=011100&title=La+relation+de+Chasles){ .md-button target="_blank" rel="noopener" }

## II. Colinéarité de deux vecteurs

[Colinéarité et déterminant](https://coopmaths.fr/alea/?uuid=2ba42&id=can2G12&n=5&d=10&cd=1&uuid=c0d5f&id=can2G16&n=5&d=10&cd=1&uuid=ee579&id=can2G09&n=5&d=10&cd=1&v=eleve&es=011100&title=Vecteurs+colin%C3%A9aires){ .md-button target="_blank" rel="noopener" }