---
author: Mireille Coilhac
title: Crédits
---


Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).

😀 Un grand merci à  [Vincent-Xavier Jumel](https://forge.aeif.fr/vincentxavier) et [Vincent Bouillot](https://gitlab.com/bouillotvincent) qui ont réalisé la partie technique de ce site, et qui m'ont beaucoup aidé pour les mises à jour.

😀 Un grand merci à Nicolas Revéret qui a codé la représentation des fractions.