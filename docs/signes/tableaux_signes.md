---
author: Mireille COILHAC
title: Tableaux de signes et inéquations
---

## Entraînement à la résolution des inéquations : 

Les corrections proposées donnent des résolutions par calcul pour remplir les tableaux de signes. Vous pouvez les regarder, mais ce n'est pas nécessaire de le faire, car vous pouvez directement utiliser le sens de variation des fonctions affines pour remplir les + et - dans les tableaux de signes, comme nous l'avons fait en classe.


[Inéquations produits](https://coopmaths.fr/alea/?uuid=014a4&id=2N61-2&n=4&d=10&s=6&alea=5HHm&i=1&cd=1&v=eleve&es=011100
){ .md-button target="_blank" rel="noopener" }

[Inéquations quotients](https://coopmaths.fr/alea/?uuid=0716b&id=2N61-4&alea=t1Nw&i=1&uuid=0716b&id=2N61-4&n=4&d=10&s=6&alea=KINg&i=1&cd=1&v=eleve&title=In%C3%A9quations+quotients&es=011100){ .md-button target="_blank" rel="noopener" }

## Exercices "papiers" à télécharger

[Tableaux de signes](a_telecharger/poly_tableaux_signes.pdf){ .md-button target="_blank" rel="noopener" }

