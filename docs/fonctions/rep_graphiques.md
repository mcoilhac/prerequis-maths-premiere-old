---
author: Mireille COILHAC
title: Représentations graphiques des fonctions
---

## Une vidéo pour comprendre : 

<iframe width="560" height="315" src="https://www.youtube.com/embed/xHJNdrhzY4Q?si=0iiFssI-HSsqhJsP" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## Premiers exemples

* Vous pouvez modifier la fonction à représenter, faire l'exercice sur votre cahier, à la main, et vérifier.
* Pour écrire une puissance, utiliser la touche <kbd>^</kbd>.

[Représentation graphique](https://www.mathix.org/fonction/){ .md-button target="_blank" rel="noopener" }


## Images - antécédents - tableaux de valeurs

[Calculs pour déterminer images, antécédents, tableaux de valeurs](https://coopmaths.fr/alea/?uuid=afb2f&id=3F12-3&n=5&d=10&s=5&cd=1&uuid=ba520&id=3F10-2&n=10&d=10&s=5&s2=3&s3=1&cd=0&v=eleve&es=011100&title=){ .md-button target="_blank" rel="noopener" }

