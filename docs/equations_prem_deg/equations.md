---
author: Mireille COILHAC
title: Equations du premier degré
---

!!! info "Le principe"

    * On peut *ajouter* le même nombre des deux côtés du signe = .
    * On peut *soustraire* le même nombre des deux côtés du signe = .
    * On peut *multiplier* par le même nombre chaque expression se trouvant des deux côtés du signe = .
    * On peut *diviser* par le même nombre (différent de 0) chaque expression se trouvant des deux côtés du signe = .

    ??? note "On peut aussi ..."

        😢 On ne peut **rien** faire d'autre ... 

        ⛔ "Passer de l'autre côté" n'est pas une opération mathématique.


[Entraînement sur le choix des opérations pour résoudre des équations du premier degré](
https://coopmaths.fr/alea/?uuid=equations&v=eleve&es=011100&title=Op%C3%A9rations+pour+r%C3%A9soudre+une+%C3%A9quation){ .md-button target="_blank" rel="noopener" }

<iframe width="560" height="315" src="https://www.youtube.com/embed/hSbHGvQO8Ts?si=NVMwKAerlq0fIfGl" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

[Entraînement sur la résolution des équations du premier degré](https://coopmaths.fr/alea/?uuid=d02da&id=2N51-4&n=20&d=10&s=true&s2=1-2-3-4-5-6-7-8&s3=false&cd=1&v=eleve&es=011100&title=Equations){ .md-button target="_blank" rel="noopener" }



