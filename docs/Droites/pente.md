---
author: Mireille COILHAC
title: Coefficient directeur d'une droite
---


## I. Lire la pente d'une droite : 

!!! info "Mon info"

    La **pente** d'une droite s'appelle aussi son **coefficient directeur**


[Vidéo](https://fr.khanacademy.org/math/be-4eme-secondaire2/x213a6fc6f6c9e122:geometrie-analytique-la-droite/x213a6fc6f6c9e122:le-coefficient-directeur-la-pente-d-une-droite/v/slope-of-a-line){ .md-button target="_blank" rel="noopener" }



[Entraînement](https://fr.khanacademy.org/math/be-4eme-secondaire2/x213a6fc6f6c9e122:geometrie-analytique-la-droite/x213a6fc6f6c9e122:le-coefficient-directeur-la-pente-d-une-droite/e/slope-from-a-graph){ .md-button target="_blank" rel="noopener" }

## Tracer une droite passant par un point dont on connait la pente

[Entraînement](https://fr.khanacademy.org/math/be-4eme-secondaire2/x213a6fc6f6c9e122:geometrie-analytique-la-droite/x213a6fc6f6c9e122:tracer-une-droite-a-partir-des-donnees-de-l-enonce/e/graphing-slope){ .md-button target="_blank" rel="noopener" }

## Exercices "papiers" à télécharger

[Coefficient directeur et droites](a_telecharger/poly_droites.pdf){ .md-button target="_blank" rel="noopener" }







